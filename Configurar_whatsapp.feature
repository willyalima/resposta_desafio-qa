Funcionalidade:  Reduzir uso de dados

O usuário pode acessar a seção de configurações do whatsapp onde terá a opção de configurar os consumos de uso de dados do aplicativo.
O usuário desabilita a confirmação de leitura de mensagens enviadas


 Cenário: Se o usuário desejar reduzir o consumo de dados  através de desabilitar a função em configurações.
 Dado que estou na tela de conversas do whatsapp
 E naveguei ate a tela de configurações 
 Então posso ver as opções de configurações do aplicativo
 
 Cenário: Ao selecionar a opção de Uso de Dados, o usuário poderá visualizar opções de configurações referente a consumo de dados, podendo ativar a opções de redução de uso de dados
 Dado que estou na tela de configurações
 Quando seleciono a opção de Uso de Dados e armazenamento
 E visualizo as opções de configurações de Uso de Dados
 Então posso ativar a opção de Redução de Uso de Dados

 Cenário: Habilitar o download automatico de fotos somente pelo wifi
 Dado que estou na tela de configurações 
 Quando seleciono a opção de Uso de Dados e Armazenamento
 E seleciono a opção fotos 
 Então posso habilitar a opção Wifi 
 


 


