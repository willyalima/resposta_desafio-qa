 Funcionalidade:  Ativação de itens de privacidade

1- O usuário pode acessar as configurações para desabilitar a confirmação de leitura de mensagens. 
2- O usuário pode definir somente contatos para visualiarem sua foto de perfil.


 Cenário: O usuário opta por desabilitar a opção de Confirmação de Leitura de mensagens na opção de Privacidades 
 Dado que estou na tela de ajustes 
 E navego ate a opção Conta
 Quando seleciono a opção Privacidade
 Então posso desativar a opção de Confirmação de Leitura

 Cenário: O usuário opta por ter sua foto de perfil vista somente por apenas seus contatos
 Dado que estou na tela de ajustes 
 E navego ate a opção Conta 
 Quando seleciono a opção Privacidade 
 E seleciono a opção foto de perfil
 Então posso escolher a opção de apenas contatos 