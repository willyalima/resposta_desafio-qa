class CheckOut

    #inicialização das variaveis.
    def initialize products, discount_rules
        @products, @discount_rules = products, discount_rules
        @items = ""
    end

    def scan item
        if !@products.include? item 
            # Não foi possível adicionar o produto #{item}          
            return
        end
        @items += item
        #puts "Produto #{item} adicionado"             
        #puts "Carrinho atual #{@items}"
    end

    def total
        total = 0
        @products.each do |item, price| 
            qtd = @items.chars.grep(item).size
            total += item_total_price_calculator(item, price, qtd)
        end
        return total 
    end

    # Método que calcula o valor total de um item no carrinho aplicando as regras de descontos
    # 
    # Params:
    # - item: Representa um item adicionado ao carrinho
    # - price: Representa o valor unitário do item
    # - qtd: Quantidade do item no carrinho 
    #
    # Returns:
    # - O valor total do item com as regras de descontos aplicadas
    private def item_total_price_calculator item, price, qtd
        item_total_price = 0
        item_rules = Hash[]

        item_rules = @discount_rules[item] if @discount_rules.include? item

        item_rules.each do |rule_qtd, rule_price| 
            number_of_promotions = qtd / rule_qtd
            item_total_price += number_of_promotions * rule_price
            qtd = qtd % rule_qtd
        end

        item_total_price += price * qtd

        return item_total_price
    end
    
end
